let array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3.", "1.2.3."]

let object_code = {}

for (let i = 0; i < array_code.length; i++) {
    let str = array_code[i]
    
    if(str.length==4){
        object_code[str[0]] = {
            ...object_code[str[0]],
            [str[2]]: {}
        }
    } else if(str.length==6){
        object_code[str[0]] = {
            ...object_code[str[0]],
            [str[2]] : {
                ...object_code[str[0]][str[2]],
                [str[4]] : str
            }
        }
    }

}

let output = JSON.stringify(object_code,null, 3)

console.log(output)