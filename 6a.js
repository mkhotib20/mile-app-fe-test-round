// 6.a Swap the values of variables A and B
let A = 3
let B = 5

A = A+B
B = A-B
A = A-B

console.log("New A Value : ", A)
console.log("New B Value : ", B)