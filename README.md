# Mile App FE Test Round

## 1. Do you prefer vuejs or reactjs? Why ?
For me, i prefer reactjs. because : 

- based on the literacy that I have read before, i found that reactjs community larger than vuejs and library availability in npm is more than vuejs.

- I am more flexible in coding, because, reactjs coding style like OOP, i can use class based component with strutured function inside like class in general OOP.

- reactjs just like vanilla javascript, all JS standards applied in reactjs. Because, i learned javascript before learning the frontend library. So, i can adapt easily in reactjs.

## 2. What complex things have you done in frontend development ?
![Image](https://i.ibb.co/6Z8YFTF/image.png)

I've created a nested form with an array depth of about 6 array levels. each input has children and can add more children afterwards up to 6 levels. 

## 3. why does a UI Developer need to know and understand UX? how far do you understand it?
A UI developer need to understand UX properly because, understanding UX will make the application easy to use with the simplest flow. my understanding of UX is limited to UX which is common used with other apps. Usually, I adapt the ux from another application and cut the flow to make it more simple if needed.

## 4. Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!

- first I want to give a feedback to a gray color on top. the gray color with light blue in the main tone of the color i feel doesn't blend well. you can change the color with light blue to.
![Image](https://i.ibb.co/jHZv8tn/image.png)

- the font size from login box is smaller than the top bar font size. I think the main point of this page is login and the message must be delivered properly

- The margin between copyright text is too close to the bottom border of the document
![Image](https://i.ibb.co/wwJztSz/image.png)

- The form feedback from the previous error is not cleared when the content of the input have changed. I think this can confuse the user because they think that what they are inputting is still wrong.<br/>
![Image](https://i.ibb.co/YBFm5kX/image.png)

- I think, adding a loading spinner inside the button can make users think they need to wait while submitting the organization name

## 5. Create a better login page based on https://taskdev.mile.app/login and deploy on https://www.netlify.com (https://www.netlify.com/)!

I've created and deployed login page on Netlify, you can check in this link https://mile-login.netlify.app/ 
Gitlab repo : https://gitlab.com/mkhotib20/mile-app-test-login-page

## 6. Solve the logic problems below !
I have put the answer in a separate file and can be run with the node command